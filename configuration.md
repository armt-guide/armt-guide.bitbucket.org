# Creating a New Cellular Configuration Profile

**Notice:** Configurations can only be managed by admin users.  Please ensure that the appropriate user has the correct permissions setup.  Refer to the [Managing User Permissions](gitbook/profile/permissions.md) section of this guide for details on how to setup the correct permissions.

Before creating a new cellular configuration profile, first decide if the profile should inherit any settings from another configuration profile (referred to as the 'parent configuration').  This adds the benefit of having a structured and/or tiered configuration environment, where a new configuration inherits all the defaults for an organization, and the user adds the necessary changes to those base settings.  To view the current list of configuration profiles a user has access to, click on the `Configurations` button in the navigation panel.  Note that if a user does not select a parent configuration, the profile will inherit the system default values for a cellular device. Refer to the help boxes for each configuration option for details on what each default its value is.

![New Cellular Configuration](http://downloads.accns.com/aview/new_cellular_config.png)

To create a new configuration profile, first navigate to the `Configurations` section of the AView site by clicking on the `Configurations` button in the navigation panel.  Next click the plus icon located at the top right of the window to create a new cellular configuration.  For details of each configuration option, refer to the individual device configuration section(s) of this guide.  Note that required fields are marked with asterisks (*) and any values inherited from the parent configuration are listed in red.  Once all the desired configuration options have been specified, click `Create`.

## Applying the new configuration profile

To apply the new configuration profile to a device, go to the device details page for the specific device.  This can be done by searching for the device's MAC address in the search bar, then clicking on the MAC address of the correct device in the results.  Once on the details page, select the new configuration profile from the `Configuration` drop-down menu in the Admin panel.

![Apply a Cellular Configuration](http://downloads.accns.com/aview/applying_cellular_config.png)
