# Notifications

Displays the last 24 hours of notifications that were sent to the user based on their notification subscriptions.  For details on notification subscriptions, refer to the [Managing Notifications](gitbook/notifications/README.md) section of this guide.

![Notifications](http://downloads.accns.com/aview/notifications.png)

The Notification page is a convenient way to look back at alerts that were sent to the user, in case they deleted the email or need to have an extra record of it. The information column sums up the content of the email for quick reference and when clicking on it, takes the user to the event that spawned the email for more analysis and information. The user can filter just like events and go back as far as 30 days. The date displayed is when the notification was generated.  For details on the filter types and the information listed in the table, refer to the [Events](gitbook/site/events.md) section of this guide.